## Sberbank REST API integration

### Installation
Install this package with composer:
```
composer require efremovp/sberbank-rest
```

### Usage
```
use efremovP\Sberbank\SberBankClient;
use efremovP\Sberbank\Models\Request\Order;
use efremovP\Sberbank\Models\Request\OrderPosition;
```

Create new order in bank
```
$bank = new SberBankClient('login-api', 'password-api', true);
// register order
$res = $bank->getPaymentUrl(new Order('123', 'test', 500, 'descr'), 'http://app.com');
```

### Fiscalization (optional)
 
If you use fiscalization via Sberbank, you need send items in order

```
$order = new Order('123', 'test', 500, 'descr', 'customer@email.com', [
    new OrderPosition('123', 'test', 500, 'descr')
]);
$res = $bank->getPaymentUrl($order, 'http://app.com');

```

Check order status
```
// get order status
$status = $bank->checkStatus('sberbank order id', 'order id in your system');
```

### Logging actions (optional)

You can use custom logger for this package. Just create logger class implement from package LoggerContract interface.

```
use efremovP\Sberbank\Logger\LoggerContract;

class MyLog implements LoggerContract
{
    public static function push(...);
}

$logger = new MyLog();

$bank = new SberBankClient('login-api', 'password-api', true, $logger);

``` 

### Test cards for check integration

https://securepayments.sberbank.ru/wiki/doku.php/integration:api:rest:start

### About SSL sertificate russian trusted
https://securepayments.sberbank.ru/wiki/doku.php/certificates:start

### How to install russiantrusted ca-certificates
https://interface31.ru/tech_it/2022/10/ustanovka-rossiyskih-kornevyh-sertifikatov-v-debian-i-ubuntu.html

### Telegram channel about PHP packages
Subscribe - https://t.me/php_package

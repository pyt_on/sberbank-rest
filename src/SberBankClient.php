<?php

namespace efremovP\Sberbank;

use efremovP\Sberbank\Logger\LoggerContract;
use efremovP\Sberbank\Models\Request\Order;
use efremovP\Sberbank\Models\Request\OrderPosition;
use efremovP\Sberbank\Models\Response\CheckStatusResponse;
use efremovP\Sberbank\Models\Response\PaymentUrlResponse;

class SberBankClient
{
	private $testMode;
	const SUCCESS_PAYMENT = 1;
	/*
	 * Подробнее по доменам https://securepayments.sberbank.ru/wiki/doku.php/certificates:start
	 *
	 * Установка сертификатов Минцифры https://interface31.ru/tech_it/2022/10/ustanovka-rossiyskih-kornevyh-sertifikatov-v-debian-i-ubuntu.html
	 * 
	 * Проверка сертификата:
	 *   apt-get install p11-kit
	 *   trust list
 	 *
	 * Список доступных альтернативных доменов для ПС с сертификатом НУЦ Минцифры
	 *  https://3dsec.sberbank.ru
	 * Список доступных альтернативных доменов для ПС: (c сертификатами международных УЦ)
	 *  https://secure-payment-gateway.ru/
	*/
	const TEST_BANK_SERVER = 'https://secure-payment-gateway.ru/payment/rest';
	/*
	 * Список доступных альтернативных доменов для ПС с сертификатом НУЦ Минцифры
	 *   https://securepayments.sberbank.ru
	 * Список доступных альтернативных доменов для ПС: (c сертификатами международных УЦ)
	 *   securecardpayment.ru
	 *   secure-card-payment.ru
	 *   securepaymentway.ru
	 *   secure-payment-way.ru
	 *   securepaymentgateway.ru
	 */
	const PRODUCTION_BANK_SERVER = 'https://securepayments.sberbank.ru/payment/rest';
	private $bankRegisterUrl = '/register.do';
	private $bankCheckUrl = '/getOrderStatusExtended.do'; // получаем расширенные даныне
	public $redirectOrderIdName = 'orderId';
	private $login;
	private $password;
	protected $redirectUrl;
	protected $logger;

	private $methodNames = [
		'getPaymentUrl' => 'Получение формы оплаты',
		'checkStatus' => 'Проверка статуса заказа'
	];

	const SUCCESS_TRANSACTION_CODES = [
		1, 2
	];

	/**
	 * Содержит коды ошибок банка и их расшифровки
	 * @var array
	 */
	protected $responseCodes = [
		'0' => 'Обработка запроса прошла без системных ошибок',
		'2' => 'Заказ отклонен по причине ошибки в реквизитах платежа',
		'5' => 'Доступ запрещён, Пользователь должен сменить свой пароль, Номер заказа не указан',
		'6' => 'Неизвестный номер заказа',
		'7' => 'Системная ошибка'
	];

	protected $statusCodes = [
		'0' => 'Заказ зарегистрирован, но не оплачен.',
		'1' => 'Предавторизованная сумма захолдирована. Платеж прошел успешно',
		'2' => 'Проведена полная авторизация суммы заказа. Платеж прошел успешно',
		'3' => 'Операция отменена.',
		'4' => 'По транзакции была проведена операция возврата.',
		'5' => 'Инициирована авторизация через ACS банка-эмитента.',
		'6' => 'Операция отклонена.'
	];

	public function __construct($login, $password, $testMode = false, LoggerContract $logger = null)
	{
		$this->login = $login;
		$this->password = $password;
		$this->testMode = $testMode;
		$this->logger = $logger;
	}


	protected function getActionText($alias)
	{
		if (array_key_exists($alias, $this->methodNames))
			return $this->methodNames[$alias];
		return 'неизвестное действие';
	}

	/**
	 * Запрос платежной страницы от банка
	 * @param Order $order
	 * @return PaymentUrlResponse
	 */
	public function getPaymentUrl(Order $order, $redirectUrl)
	{
		$this->redirectUrl = $redirectUrl;
		$iOrderId = $order->id;
		$this->redirectUrl .= '?' . http_build_query([
				'orderNumber' => $iOrderId
			]);
		$data = [
			'returnUrl' => $this->redirectUrl,
			//'currency' => $order->currency,
			'orderNumber' => $iOrderId . '-' . time(),
			'amount' => $order->amount,
			'description' => $order->description
		];
		if ($order->items) {
			$data['orderBundle'] = json_encode($this->getCart($order));
		}

		if ($order->extraData) {
			$data['jsonParams'] = json_encode($this->extraData($order->extraData));
		}

		$jsonResult = $this->gateway($this->bankRegisterUrl, $data);
		$arResult = json_decode($jsonResult, true);
		$sberOrderId = $arResult['orderId'] ?? 0;
		$bankResponse = new PaymentUrlResponse();
		$bankResponse->jsonResponse = $jsonResult;
		if (!empty($arResult["formUrl"])) {               // Если у нас есть URL для оплаты, то...
			$bankResponse->fill([
				'error' => false,
				'paymentUrl' => $arResult['formUrl'],
				'bankOrderId' => $sberOrderId
			]);
			$this->log($sberOrderId, $iOrderId, $this->getActionText(__FUNCTION__), [
				'code' => 0,
				'statusText' => 'ОК'
			], $jsonResult);
		} else {
			$bankResponse->fill([
				'error' => true,
				'errorCode' => $arResult['errorCode'] ?? 0,
				'errorMessage' => $arResult['errorMessage'] ?? 'неизвестная ошибка'
			]);
			$this->log($sberOrderId, $iOrderId, $this->getActionText(__FUNCTION__), [
				'code' => (!empty($arResult['errorCode'])) ? $arResult['errorCode'] : 0,
				'statusText' => (!empty($arResult['errorMessage'])) ? $arResult['errorMessage'] : 'неизвестная ошибка'
			], $jsonResult);
		}
		return $bankResponse;
	}

	/**
	 * Проверяет статус оплаты в банке
	 * если в возвращаемом массиве order_status_code === 1 значит оплата прошла успешно
	 * иначе нет
	 * @param $alfOrderId
	 * @param $paymentId
	 * @return CheckStatusResponse
	 */
	public function checkStatus($alfOrderId, $paymentId)
	{
		$data = [
			'orderId' => $alfOrderId
		];
		$jsonResult = $this->gateway($this->bankCheckUrl, $data);
		$arResult = json_decode($jsonResult, true);
		$bankResponse = new CheckStatusResponse();
		$bankResponse->jsonResponse = $jsonResult;
		$bankResponse->statusCode = $arResult['errorCode'];
		if (isset($arResult['orderStatus'])) {

			$this->log($alfOrderId, $paymentId, $this->getActionText(__FUNCTION__), [
				'code' => $arResult['errorCode'],
				'statusText' => $this->getStatusByCode($arResult['orderStatus']),
			], $jsonResult);

			$orderStatusText = '';
			if (in_array($arResult['orderStatus'], self::SUCCESS_TRANSACTION_CODES)) { // оплата завершена успешно
				$arResult['orderStatus'] = self::SUCCESS_PAYMENT;
				$orderStatusText = isset($arResult['actionCodeDescription']) && $arResult['actionCodeDescription'] ?
					$arResult['actionCodeDescription'] : $this->getStatusByCode($arResult['orderStatus']);
			}
			$bankResponse->status = true;
			$bankResponse->orderId = $paymentId;
			$bankResponse->orderStatus = !$orderStatusText ? $this->getStatusByCode($arResult['orderStatus']) : $orderStatusText;
			$bankResponse->orderStatusCode = $arResult['orderStatus'];
			$bankResponse->statusText = $this->getStatusByCode($arResult['orderStatus']);
			return $bankResponse;
		}

		$this->log($alfOrderId, $paymentId, $this->getActionText(__FUNCTION__), [
			'code' => $arResult['errorCode'],
			'statusText' => $this->getErrorByCode($arResult['errorCode'])
		], $jsonResult);

		$bankResponse->fill([
			'statusText' => $this->getErrorByCode($arResult['errorCode']),
			'orderId' => $paymentId,
			'status' => false,
			'orderStatusCode' => (int)!self::SUCCESS_PAYMENT
		]);
		return $bankResponse;
	}

	/**
	 * Возвращает ошибку по коду
	 * @param $errorCode
	 * @return mixed|string
	 */
	public function getErrorByCode($errorCode)
	{
		if (array_key_exists($errorCode, $this->responseCodes)) {
			return $this->responseCodes[$errorCode];
		}
		return 'Неизвестная ошибка';
	}

	/**
	 * Формируем адрес сервера банка
	 * @return string
	 */
	public function getBankServer()
	{
		return $this->testMode ? self::TEST_BANK_SERVER : self::PRODUCTION_BANK_SERVER;
	}

	/**
	 * Выполняет запрос в банк
	 * @param $method
	 * @param $data
	 * @return bool|string
	 */
	private function gateway($method, $data)
	{
		$data['userName'] = $this->login;
		$data['password'] = $this->password;
		$curl = curl_init(); // Инициализируем запрос
		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->getBankServer() . $method, // Полный адрес метода
			CURLOPT_RETURNTRANSFER => true, // Возвращать ответ
			CURLOPT_POST => true, // Метод POST
			CURLOPT_POSTFIELDS => http_build_query($data) // Данные в запросе
		));
		$response = curl_exec($curl); // Выполненяем запрос
		curl_close($curl); // Закрываем соединение
		return $response; // Возвращаем ответ
	}

	/**
	 * Возвращает статус запроса по коду от банка
	 * @param $statusCode
	 * @return mixed|string
	 */
	private function getStatusByCode($statusCode)
	{
		if (array_key_exists($statusCode, $this->statusCodes)) {
			return $this->statusCodes[$statusCode];
		}
		return 'Неизвестный статус';
	}

	/**
	 * Корзина заказа
	 * @param Order $order
	 * @return array
	 */
	private function getCart(Order $order)
	{
		$items = [];
		$cart = [
			'customerDetails' => [
				'email' => $order->customerEmail
			]
		];

		foreach ($order->items as $k => $item) {
			/**
			 * @var OrderPosition $item
			 */
			$items[] = [
				'positionId' => $k,
				'name' => $item->name,
				'quantity' => [
					'value' => $item->quantity,
					'measure' => $item->measure
				],
				'itemCode' => $item->id,
				'tax' => [
					'taxType' => $item->tax
				],
				'itemPrice' => round($item->amount)
			];
		}

		$cart['cartItems']['items'] = $items;

		return $cart;
	}

	/**
	 * Формирует расширенные данные заказа
	 * @param array $extraData
	 * @return array
	 */
	private function extraData(array $extraData)
	{
		return $extraData;
	}

	private function log($orderId, $paymentId, $typeAction, array $response, $fullJsonResponse = "")
	{
		if ($this->logger) {
			$this->logger::push($orderId, $paymentId, $typeAction, $response, $fullJsonResponse);
		}
	}
}

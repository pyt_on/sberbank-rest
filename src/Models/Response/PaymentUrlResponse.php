<?php

namespace efremovP\Sberbank\Models\Response;

use efremovP\Sberbank\Models\BankModel;

class PaymentUrlResponse extends BankModel
{
	public $error;

	public $paymentUrl;

	public $bankOrderId;

	public $errorCode;

	public $errorMessage;

	public $jsonResponse;
}

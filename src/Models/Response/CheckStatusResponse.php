<?php namespace efremovP\Sberbank\Models\Response;

use efremovP\Sberbank\Models\BankModel;

class CheckStatusResponse extends BankModel
{
	public $status;

	public $statusCode;

	public $statusText;

	public $orderId;

	public $orderStatus;

	/**
	 * Define order payment status
	 * @var
	 */
	public $orderStatusCode;

	public $jsonResponse;
}

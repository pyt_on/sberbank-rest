<?php

namespace efremovP\Sberbank\Models;


class BankModel
{
	protected $guarded = [];

	public function fill(array $attributes)
	{
		foreach ($attributes as $k => $val) {
			if (property_exists($this, $k) && !in_array($k, $this->guarded)) {
				$this->{$k} = $val;
			}
		}
	}
}

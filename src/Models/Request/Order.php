<?php

namespace efremovP\Sberbank\Models\Request;

use efremovP\Sberbank\Models\BankModel;

class Order extends BankModel
{
	protected $guarded = [
		'amount'
	];

	const AMOUNT_RATIO = 100;

	/**
	 * Currency RUB
	 */
	const CURRENCY = 810;

	/**
	 * Internal order id
	 * @var
	 */
	public $id;

	/**
	 * Amount of order
	 * @var float|int
	 */
	public $amount;

	/**
	 * Order name
	 * @var string
	 */
	public $name;

	/**
	 * Order description
	 * @var string
	 */
	public $description;

	/**
	 * Customer email
	 * @var string
	 */
	public $customerEmail;

	/**
	 * @var OrderPosition[]
	 */
	public $items;

	public $currency;

	/**
	 * Extra data for hotels
	 * @var array
	 */
	public $extraData;

	public function __construct($id, $name, $amount, $description, $customerEmail = 'admin@mail.ru', array $items = [], $currency = self::CURRENCY, $extraData = [])
	{
		$this->id = $id;
		$this->amount = ((float)$amount) * self::AMOUNT_RATIO;
		$this->name = $name;
		$this->description = $description;
		$this->customerEmail = $customerEmail;
		$this->items = $items;
		$this->currency = $currency;
		$this->extraData = $extraData;
	}
}

<?php

namespace efremovP\Sberbank\Models\Request;

use efremovP\Sberbank\Models\BankModel;

class OrderPosition extends BankModel
{
	protected $guarded = [
		'amount'
	];

	public $id;

	public $amount;

	/**
	 *  без НДС
	 */
	const TAX_TYPE_0 = 0;

	/**
	 * НДС 20%
	 */
	const TAX_TYPE_20 = 6;

	public $name;

	public $tax;

	public $quantity;

	public $description;

	public $measure;

	public function __construct($id, $name, $amount, $quantity = 1, $description, $measure = 'штук', $tax = self::TAX_TYPE_0)
	{
		$this->id = $id;
		$this->amount = ((float)$amount) * Order::AMOUNT_RATIO;
		$this->name = $name;
		$this->description = $description;
		$this->tax = $tax;
		$this->quantity = $quantity;
		$this->measure = $measure;
	}
}
